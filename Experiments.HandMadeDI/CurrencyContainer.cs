﻿namespace Experiments.HandMadeDI
{
    using System.Configuration;

    internal class CurrencyContainer
    {
        public CurrencyParser ResolveCurrencyParser()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CommerceObjectContext"].ConnectionString;

            CurrencyProvider provider = new SqlCurrencyProvider(connectionString);

            return new CurrencyParser(provider);
        }
    }
}
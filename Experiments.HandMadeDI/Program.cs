﻿namespace Experiments.HandMadeDI
{
    using System.Configuration;

    class Program
    {
        static void Main(string[] args)
        {
            var container = new CurrencyContainer();
            container.ResolveCurrencyParser().Parse(args).Execute();
        }
    }

    internal class CurrencyContainer
    {
        public CurrencyParser ResolveCurrencyParser()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CommerceObjectContext"].ConnectionString;

            CurrencyProvider provider = new sqlCurrencyProvider(connectionString);

            return new CurrencyParser(provider);
        }
    }

    internal class CurrencyParser
    {
        public CurrencyParser(CurrencyProvider provider)
        {
            throw new System.NotImplementedException();
        }

        public void Parse(string[] args)
        {
            

        }
    }

    internal class CurrencyProvider
    {
    }
}

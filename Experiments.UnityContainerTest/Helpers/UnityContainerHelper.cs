﻿namespace Experiments.UnityContainerTest
{
    using System;
    using Microsoft.Practices.Unity;

    public static class UnityContainerHelper
    {
        public static void RegisterContainerControlled<TInterface, TInstance>(this UnityContainer container)
    where TInstance : TInterface
        {
            container.RegisterType<TInterface, TInstance>(new ContainerControlledLifetimeManager());
        }

        public static void RegisterContainerControlled<TInstance>(this UnityContainer container)
        {
            container.RegisterType<TInstance>(new ContainerControlledLifetimeManager());
        }

        public static void RegisterPerResolve<TInterface, TInstance>(this UnityContainer container)
            where TInstance : TInterface
        {
            container.RegisterType<TInterface, TInstance>(new PerResolveLifetimeManager());
        }

        public static void RegisterPerResolve<TInstance>(this UnityContainer container)
        {
            container.RegisterType<TInstance>(new PerResolveLifetimeManager());
        }

        public static void RegisterFuncFactory<TInterface, TInstance>(this UnityContainer container)
            where TInstance : TInterface
        {
            container.RegisterType<Func<TInterface>>(
                new InjectionFactory(i => new Func<TInterface>(() => container.Resolve<TInterface>())));

            container.RegisterType<Func<ResolverOverride[], TInterface>>(
                new InjectionFactory(i => new Func<ResolverOverride[], TInterface>(container.Resolve<TInterface>)));

            container.RegisterType<Func<string, ResolverOverride[], TInterface>>(
                new InjectionFactory(i => new Func<string, ResolverOverride[], TInterface>(container.Resolve<TInterface>)));
        }

        public static void RegisterFuncFactory<TInstance>(this UnityContainer container)
        {
            container.RegisterType<Func<TInstance>>(
                new InjectionFactory(i => new Func<TInstance>(() => container.Resolve<TInstance>())));

            container.RegisterType<Func<ResolverOverride[], TInstance>>(
                new InjectionFactory(i => new Func<ResolverOverride[], TInstance>(container.Resolve<TInstance>)));

            container.RegisterType<Func<string, ResolverOverride[], TInstance>>(
                new InjectionFactory(i => new Func<string, ResolverOverride[], TInstance>(container.Resolve<TInstance>)));
        }        
    }


}

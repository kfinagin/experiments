﻿namespace Experiments.UnityContainerTest
{
    using System;
    using System.Runtime.CompilerServices;
    using Experiments.TestClasses.ExperimentClasses;
    using Microsoft.Practices.Unity;

    class Program
    {
        static UnityContainer container;


        static void Main(string[] args)
        {
            container = new UnityContainer();
            
            container.RegisterPerResolve<ITest1, Test1>();

            container.RegisterPerResolve<ITest2, Test2WithDependency>();
            container.RegisterFuncFactory<ITest2, Test2WithDependency>();

            container.RegisterPerResolve<ITest3, Test3WithDependency>();
            container.RegisterFuncFactory<ITest3, Test3WithDependency>();

            container.RegisterType<ITest4, Test4WithDependency>(
                "someRuntimeData1Constructor",
                new InjectionConstructor(typeof (SomeRuntimeData1), typeof (ITest1))
                );

            container.RegisterType<ITest4, Test4WithDependency>(
                "someRuntimeData2Constructor",
                new InjectionConstructor(typeof (SomeRuntimeData2), typeof (ITest1))
                );

            container.RegisterFuncFactory<ITest4, Test4WithDependency>();

            container.RegisterPerResolve<SomeComplexFactory>();

            var result = container.Resolve<SomeComplexFactory>();
        }
        
    }
}

﻿namespace Experiments.UnityContainerTest
{
    using System;
    using Experiments.TestClasses.ExperimentClasses;
    using Microsoft.Practices.Unity;

    public class SomeComplexFactory
    {
        private ITest2 test2;

        private ITest3 test3;

        private ITest4 test4;

        public SomeComplexFactory(
            Func<ResolverOverride[], ITest2> builder2Func,
            Func<ResolverOverride[], ITest3> builder3Func,
            Func<string, ResolverOverride[], ITest4> builder4Func 
            )
        {
            this.test2 = builder2Func.CallWithUnityParametersOverride(new SomeRuntimeData1(1, "string1"));

            this.test3 = builder3Func.CallWithUnityParametersOverride(new SomeRuntimeData1(2, "string2"), new SomeRuntimeData2(3,"string3"));

            this.test4 = builder4Func.CallWithUnityParametersOverride(new SomeRuntimeData1(3, "string3"), "someRuntimeData1Constructor");
        }
    }
}
    
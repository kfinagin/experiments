﻿namespace ReactiveExtensions.ConsoleTest.CreatingSequences
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reactive.Linq;

    public class FunctionalUnfolds
    {
        // Corecursion
        private static IEnumerable<T> Unfold<T>(T seed, Func<T, T> accumulator)
        {
            var nextValue = seed;
            while (true)
            {
                yield return nextValue;
                nextValue = accumulator(nextValue);
            }
        }

        // Observable.Generate sample
        public static IObservable<int> Range(int start, int count)
        {
            var max = start + count;
            return Observable.Generate(
                start,
                value => value < max, // condition
                value => value + 1, // iterate
                value => value // result selector
                );
        } 

        public void TestUnfold()
        {
            var naturalNumbers = Unfold(1, i => i + 1);

            foreach (var number in naturalNumbers.Take(10))
            {
                Console.WriteLine(number);
            }
        }

        public void TestTimers()
        {
            var interval = Observable.Interval(TimeSpan.FromMilliseconds(250));
            var subscription = interval.Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Completed")
                );
            
            subscription.Dispose();
        }

        public void TestTimers2()
        {
            var timer = Observable.Timer(TimeSpan.FromSeconds(1));
            timer.Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("completed")
                );
        }

        public static IObservable<long> Timer(TimeSpan dueTime)
        {
            return Observable.Generate(
                0L,
                i => i < 1,
                i => i + 1,
                i => i,
                i => dueTime
                );
        }

        public static IObservable<long> Timer(TimeSpan dueTime, TimeSpan period)
        {
            return Observable.Generate(
                0L,
                i => true,
                i => i + 1,
                i => i,
                i => i == 0 ? dueTime : period
                );
        }

        public static IObservable<long> Interval(TimeSpan period)
        {
            return Observable.Generate(
                0L,
                i => true,
                i => i + 2,
                i => i,
                i => period
                );
        } 
    }


}

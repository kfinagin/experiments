﻿namespace ReactiveExtensions.ConsoleTest
{
    using System;
    using System.Reactive.Disposables;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    using System.Threading;
    using System.Timers;

    public class SimpleFactoryMethods
    {

        private void CreatingSequences()
        {
            var singleValue = Observable.Return<string>("Value");
            var subject = new ReplaySubject<string>();
            subject.OnNext("Value");
            subject.OnCompleted();

            var empty = Observable.Empty<string>();
            var subject2 = new ReplaySubject<string>();
            subject2.OnCompleted();

            var never = Observable.Never<string>();
            var subject3 = new Subject<string>();

            var throws = Observable.Throw<string>(new Exception());
            var subject4 = new ReplaySubject<string>();
            subject4.OnError(new Exception());



        }

        public IObservable<string> BlockingMethod()
        {
            var subject = new ReplaySubject<string>();

            subject.OnNext("a");
            subject.OnNext("b");
            subject.OnCompleted();

            Thread.Sleep(1000);

            return subject;
        }
        
        // Observable.Create provides you a lazily evaluated way to create observable sequences
        public IObservable<string> NonBlocking()
        {
            return Observable.Create<string>(
                (IObserver<string> observer) =>
                {
                    observer.OnNext("a");
                    observer.OnNext("b");
                    observer.OnCompleted();

                    Thread.Sleep(1000);

                    return Disposable.Create(() => Console.WriteLine("Observer has unsubscribed"));

                    // return () => Console.WriteLine("Observer has unsubscribed");
                });

        }

        // example code only - wrong
        public void NonBlockingEventDriven()
        {
            var ob = Observable.Create<string>(observer =>
            {
                var timer = new System.Timers.Timer();
                timer.Interval = 1000;
                timer.Elapsed += (s, e) => observer.OnNext("tick");
                timer.Elapsed += OnTimerElapsed;
                timer.Start();
                return Disposable.Empty;
            });

            var subscription = ob.Subscribe(Console.WriteLine);
            Console.ReadLine();
            subscription.Dispose();
        }

        public void NonBlockingEventDrivenFixed()
        {
            var ob = Observable.Create<string>(observer =>
            {
                var timer = new System.Timers.Timer();
                timer.Interval = 1000;
                timer.Elapsed += (s, e) => observer.OnNext("tick");
                timer.Elapsed += OnTimerElapsed;
                timer.Start();

                // returning timer as disposable token
                return timer;
            });

            var subscription = ob.Subscribe(Console.WriteLine);
            Console.ReadLine();
            subscription.Dispose();
        }


        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine(e.SignalTime);
        }
    }

}

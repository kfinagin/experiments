﻿namespace ReactiveExtensions.ConsoleTest.CreatingSequences
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Net.Mime;
    using System.Reactive.Disposables;
    using System.Reactive.Linq;
    using System.Reactive.Threading.Tasks;
    using System.Runtime.ExceptionServices;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public class TransitioningToIObservable
    {
        public static void StartAction()
        {
            var start = Observable.Start(() =>
            {
                Console.WriteLine("Working away");
                for (int i = 0; i < 10; i++)
                {
                    Thread.Sleep(100);
                    Console.Write(".");
                }
            });

            // unit type is analogous to void - functional programming construct - empty payload
            start.Subscribe(
                unit => Console.WriteLine("Unit published"), 
                () => Console.WriteLine("Action completed"));
        }

        public static void StartFunc()
        {
            // useStartAsync for async methods
            var start = Observable.Start(() =>
            {
                Console.WriteLine("Working away");
                for (int i = 0; i < 10; i++)
                {
                    Thread.Sleep(100);
                    Console.Write(".");
                }
                return "Published value";
            });

            start.Subscribe(
                Console.WriteLine,
                () => Console.WriteLine("Action completed")
                );
        }

        private PropertyChangedEventHandler PropertyChanged { get; set; }

        public void ObservableFromEvents()
        {
            var propChanged = Observable.FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>(
                handler => handler.Invoke,
                h => this.PropertyChanged += h,
                h => this.PropertyChanged -= h
                );
            
            var firstChanceException = Observable.FromEventPattern<FirstChanceExceptionEventArgs>(
                h => AppDomain.CurrentDomain.FirstChanceException += h,
                h => AppDomain.CurrentDomain.FirstChanceException -= h
                );
        }

        public void ObservableFromTask()
        {
            var t = Task.Factory.StartNew(() => "Test");
            var source = t.ToObservable();

            source.Subscribe(Console.WriteLine, () => Console.WriteLine("completed"));
        }

        public static IObservable<T> ToObservable<T>(/*this*/ IEnumerable<T> source)
        {
            return Observable.Create<T>(
                o =>
                {
                    foreach (var item in source)
                    {
                        o.OnNext(item);
                    }
                    return Disposable.Empty;
                });
        } 

        public void ObservableFromIEnumerable()
        {
                        
        }

    }
}

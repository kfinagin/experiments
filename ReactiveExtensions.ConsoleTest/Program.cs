﻿namespace ReactiveExtensions.ConsoleTest
{
    using System;
    using System.Reactive.Subjects;
    using ReactiveExtensions.ConsoleTest.CreatingSequences;

    class Program
    {
        static void Main(string[] args)
        {
            var timer = FunctionalUnfolds.Timer(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(1));
            var subscription = timer.Subscribe(Console.WriteLine);



            Console.ReadLine();

            subscription.Dispose();
        }

        static void WriteSequenceToConsole(IObservable<string> sequence)
        {
            sequence.Subscribe(Console.WriteLine);
        }
    }
}

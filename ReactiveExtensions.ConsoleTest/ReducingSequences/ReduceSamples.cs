﻿namespace ReactiveExtensions.ConsoleTest.ReducingSequences
{
    using System;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;

    public class ReduceSamples
    {
        public void Where1()
        {
            var oddNumbers = Observable.Range(0, 10)
                .Where(i => i%2 == 0)
                .Subscribe(Console.WriteLine, () => Console.WriteLine("Completed"));
        }

        public void Distinct()
        {
            var subject = new Subject<int>();
            var distinct = subject.Distinct();

            subject.Subscribe(
                i => Console.WriteLine("{0}", i),
                () => Console.WriteLine("subject.OnCompleted()"));


        }


    }
}

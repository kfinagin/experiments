﻿namespace ClassLibrary2
{
    using System;
    using BoDi;
    using TechTalk.SpecFlow.Generator.Configuration;
    using TechTalk.SpecFlow.Generator.Plugins;
    using TechTalk.SpecFlow.Generator.UnitTestProvider;

    public class CodedUiProviderPlugin : IGeneratorPlugin
    {
        public void RegisterDependencies(ObjectContainer container)
        {
        }

        public void RegisterCustomizations(ObjectContainer container, SpecFlowProjectConfiguration generatorConfiguration)
        {
            string unitTestProviderName = generatorConfiguration.GeneratorConfiguration.GeneratorUnitTestProvider;

            if (unitTestProviderName.Equals("mstest", StringComparison.InvariantCultureIgnoreCase) ||
                unitTestProviderName.Equals("mstest.2010", StringComparison.InvariantCultureIgnoreCase))
            {
                container.RegisterTypeAs<CodedUiGeneratorProvider, IUnitTestGeneratorProvider>();
            }


        }

        public void RegisterConfigurationDefaults(SpecFlowProjectConfiguration specFlowConfiguration)
        {
        }
    }
}

﻿namespace TestWpfProjects.EmptyCaliburnMicroProject
{
    using System.Windows;
    using Caliburn.Micro;
    using TestWpfProjects.EmptyCaliburnMicroProject.ViewModels;

    public class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }
    }
}

﻿namespace TestGallery.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;
    using PropertyChanged;

    [ImplementPropertyChanged]
    public class ShellViewModel
    {
        private List<string> urls = new List<string>()
        {
            "https://placeimg.com/1000/1000",
            "https://placeimg.com/1001/1001",
            "https://placeimg.com/1002/1002",
            "https://placeimg.com/1003/1003",
            "https://placeimg.com/1004/1004",
            "https://placeimg.com/1005/1005",
            "https://placeimg.com/1006/1006",
            "https://placeimg.com/1007/1001",
            "https://placeimg.com/1007/1002",
            "https://placeimg.com/1007/1003",
            "https://placeimg.com/1007/1004",
            "https://placeimg.com/1007/1005",
            "https://placeimg.com/1007/1006",
            "https://placeimg.com/1008/1002",
            "https://placeimg.com/1008/1003",
            "https://placeimg.com/1008/1004",
            "https://placeimg.com/1008/1005",
            "https://placeimg.com/1008/1006",
            "https://placeimg.com/1009/1002",
            "https://placeimg.com/1009/1003",
            "https://placeimg.com/1009/1004",
            "https://placeimg.com/1009/1005",
            "https://placeimg.com/1009/1006",
            "https://placeimg.com/1007/1011",
            "https://placeimg.com/1007/1012",
            "https://placeimg.com/1007/1013",
            "https://placeimg.com/1007/1014",
            "https://placeimg.com/1007/1015",
            "https://placeimg.com/1007/1016",
            "https://placeimg.com/1008/1012",
            "https://placeimg.com/1008/1013",
            "https://placeimg.com/1008/1014",
            "https://placeimg.com/1008/1015",
            "https://placeimg.com/1008/1016",
            "https://placeimg.com/1009/1012",
            "https://placeimg.com/1009/1013",
            "https://placeimg.com/1009/1014",
            "https://placeimg.com/1009/1015",
            "https://placeimg.com/1009/1016",

        };


        public ShellViewModel()
        {
            ImageUris = new ObservableCollection<GalleryImage>();
        }

        public ObservableCollection<GalleryImage> ImageUris { get; set; }

        public async Task LoadImages()
        {
            foreach (var imageUri in urls)
            {
                this.ImageUris.Add(new GalleryImage() { Url = imageUri });
            }
        }
    }

    public class GalleryImage
    {
        public string Url { get; set; }

        public BitmapImage Image { get;set; }
    }
}

﻿
// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace UITestedApplication.UI
{
    using System;
    using System.Collections.Generic;
    using Windows.ApplicationModel.Activation;
    using Windows.UI.Xaml.Controls;
    using Caliburn.Micro;
    using UITestedApplication.UI.ViewModels;

    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App
    {
        private WinRTContainer container;

        public App()
        {
            InitializeComponent();
        }

        protected override void Configure()
        {
            this.container = new WinRTContainer();
            this.container.PerRequest<ShellViewModel>();
            this.container.RegisterWinRTServices();
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = this.container.GetInstance(service, key);
            if (instance != null) return instance;

            throw new Exception("Could not locate any instances");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return this.container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            this.container.BuildUp(instance);
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            this.container.RegisterNavigationService(rootFrame);
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

    }
}

﻿namespace UITestedApplication.UI.ViewModels
{
    using System.Collections.ObjectModel;
    using PropertyChanged;

    [ImplementPropertyChanged]
    public class ShellViewModel
    {
        public ShellViewModel()
        {
            TestItems = new ObservableCollection<string>()
            {
                "Test",
                "Test",
                "Test",
                "Test",
                "Test",
                "Test",
                "Test",
                "Test",
                "Test",
                "Test",
            };
        }
        public string TestText { get; set; }

        public ObservableCollection<string> TestItems { get; set; }
 
        public void Test1()
        {
            TestText = "Test 1";
        }

        public void Test2()
        {
            TestText = "Test 2";
        }
    }
}

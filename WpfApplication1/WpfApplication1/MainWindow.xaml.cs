﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private bool collapsed = false;

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (!this.collapsed)
            {
                this.MyGrid.RowDefinitions[0].Height = 
                    new GridLength(0.8, GridUnitType.Star);
                this.MyGrid.RowDefinitions[1].Height = 
                    new GridLength(0.0, GridUnitType.Star);
                this.collapsed = true;
            }
            else
            {
                this.MyGrid.RowDefinitions[0].Height = 
                    new GridLength(0.5, GridUnitType.Star);
                this.MyGrid.RowDefinitions[1].Height = 
                    new GridLength(0.3, GridUnitType.Star);
                this.collapsed = false;
            }

        }
    }
}

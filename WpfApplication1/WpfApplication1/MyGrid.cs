﻿namespace WpfApplication1
{
    using System.Windows;
    using System.Windows.Controls;

    public class MyGrid : Grid
    {
        public static readonly DependencyProperty Hide2RowProperty = DependencyProperty.Register(
            "Hide2Row", typeof(bool), typeof(MyGrid), new PropertyMetadata(default(bool), ChangedRow));

        private static void ChangedRow(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var grid = d as Grid;
            if (grid == null) return;

            var visible = (bool) e.NewValue;
            if (visible)
            {
                grid.RowDefinitions[0].Height =
                    new GridLength(0.8, GridUnitType.Star);
                grid.RowDefinitions[1].Height =
                    new GridLength(0.0, GridUnitType.Star);

            }
            else
            {
                grid.RowDefinitions[0].Height =
                    new GridLength(0.8, GridUnitType.Star);
                grid.RowDefinitions[1].Height =
                    new GridLength(0.0, GridUnitType.Star);
            }

        }

        public bool Hide2Row
        {
            get { return (bool) GetValue(Hide2RowProperty); }
            set { SetValue(Hide2RowProperty, value); }
        }
    }
}
